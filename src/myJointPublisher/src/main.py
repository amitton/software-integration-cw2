#!/usr/bin/env python
import rospy
import math
from sensor_msgs.msg import JointState 
from std_msgs.msg import Header

def calcJointVals():

    pub = rospy.Publisher('joint_states', JointState, queue_size=10)

    rospy.init_node('my_joint_state_publisher')
    rate = rospy.Rate(10)

    print '\nMove the robot end effector to a desired position: \n'
    x = input('Enter desired x coordinate: ')
    y = input('Enter desired y coordinate - (x + y must not exceed 4): ')
    z = input('Enter desired z coordinate - (Must be between 0 and 2.3):')
    
    if (x+y) > 4:
        raise ValueError('x/y combination is out of reach')
    elif z > 2.3:
        raise ValueError('z coordinate is out of reach')

    desiredPos = [x, y, z]

    joints = JointState()
    joints.name = ["column1__arm1", "arm1__arm2", "arm2__arm3"]
    joints.header = Header()
    joints.header.stamp = rospy.Time.now()

    a1Length = 2
    a2Length = 2

    # FROM INVERSE KINEMATICS
    joint2 = math.acos((x**2 + y**2 - a1Length**2 - a2Length**2)/(2 * a1Length * a2Length))
    joint1 = math.atan(y/x) - math.atan((a2Length * math.sin(joint2))/(a1Length + a2Length *      			math.cos(joint2)))
    joint3 = z
    
    #joints.position = [math.pi/2, math.pi/2, 1]
    joints.position = [joint1, joint2, joint3]
    joints.header.stamp =rospy.Time.now()
    pub.publish(joints)

    rate.sleep()

if __name__ == '__main__':
    try:
        calcJointVals()
    except rospy.ROSInterruptException:
        pass

