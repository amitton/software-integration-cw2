Running the code in Coursework 2 - Robotic Simulation (24/02/20)
----------------------------------------------------------------

VISUALISING THE ROBOT
---------------------
To visualise the SCARA arm using rviz, run the following command from the terminal:

$ roslaunch myScaraDescription display.launch

SPECIFYING THE JOINT VALUES
---------------------------
When rviz is initially opened, an error will be raised in the side bar due to the joint values not having been specified. To define these joint values, open a new terminal window and run the following commands:

$ source devel/setup.bash
$ rosrun myJointPublisher main.py 

You will then be prompted to enter the desired x, y and z coordinates of the robot end effector. Once valid coordinates are entered, the visualisation in rviz will update and the robot will move to the desired position. 

